//Proj02 : HiperCarros #2

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// ********** variáveis públicas **********
var interf={};
var nCarro = 1; // numero do carro em apresentaçao

// *********** função principal ***********
function inic(){
    adqInterf();
    introd();
    mostraCarros();
}



// ************** processos ***************
function adqInterf(){
    interf.apres =      document.getElementById("apres");
    interf.titulo =     document.getElementById("titulo");
    interf.txt1 =       document.getElementById("txt1");
    interf.txt2 =       document.getElementById("txt2");
    interf.carro =      document.getElementById("carro");
    interf.refCarro =   document.getElementById("refCarro");
    interf.txt2 =       document.getElementById("txt2");
    interf.txt2 =       document.getElementById("txt2");
    interf.txt3 =       document.getElementById("txt3");
    interf.txt4 =       document.getElementById("txt4");
    interf.figs =       document.getElementById("figs");
    interf.especs =     document.getElementById("especs");

}

// ************* input/output *************
//apresenta a introduçao
function introd(){
    interf.titulo.innerHTML = HCtituloApres();
    interf.txt1.innerHTML = 
    '<p>' + HCparagIntrod(0) + ' </p>' +
    '<p>' + HCparagIntrod(1)+ ' </p>' +
    '<p>' + HCparagIntrod(2)+  ' </p>' ;
    interf.txt2.innerHTML = 
    '<p>' + HCparagIntrod(3) + ' </p>' +
    '<p>' + HCparagIntrod(4) + ' </p>' +
    '<p><button onclick="mostraCarros()">ver carros</button></p>';
}
//apresenta o atual mcarro
function mostraCarros(){
    var i,cod4Imgs='';//codigo HTML para 4 Imagens
    for(i=0;i<4;i++){
        cod4Imgs += '<img src="' + HCrefImagCarro(nCarro,i) + '"width="221" />';
    }
    interf.refCarro.innerHTML = 
    HCmarcaModeloCarro(nCarro) +' - ' + HCversaoCarro(nCarro);
    interf.txt3.innerHTML =   HCparagCarro(nCarro,0);
    interf.txt4.innerHTML =   HCparagCarro(nCarro,1);
    interf.figs.innerHTML = cod4Imgs;
    interf.especs.innerHTML = HCcodTabEspecs(nCarro);
}