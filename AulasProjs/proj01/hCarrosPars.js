//PRJ01: Hiper Carros #1

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

//NORMA: Segundo norma indicada pelo editor da base de dados, cada csrro tem dois parágrafos descritivos e quatro
// imagens ilustrativas

// ********** variáveis públicas **********
var PATH = "../../Recursos/hCarros/";

// *********** função principal ***********
function inic() {
    HCapresTeste();
    HClistaGeralTeste();
}

// ********* processos de parsing**********

//Número de carros da apresentação
function HCnumCarros() {
    return hCarros.length - 1;
}

//Titulo da apresentaçao
function HCtituloApres() {
    return hCarros[0].titulo;
}

//Número de paragrafos da apresentação
function HCnumParagsIntrod() {
    return hCarros[0].apres.length;
}

//Texto de um paragrafo da introduçao, dado o seu índice
function HCparagIntrod(iParag) {
    return hCarros[0].apres[iParag];
}

//Marca/Modelo de um carro, dado o seu índice
function HCmarcaModeloCarro(iCarro) {
    return hCarros[iCarro].marcaModelo;
}

//Versão de um caro, dado o seu indice
function HCversaoCarros(iCarro) {
    return hCarros[iCarro].versao;
}

//Texto de um dos dois parágrafos descritivos de um carro, dado o índice do carro e o índice do parágrafo
function HCparagCarro(iCarro, iParag) {
    return hCarros[iCarro].texto[iParag];
}

//Designação do ficheiro de uma das quatros imagens ilustrativas de um carro, dado índice do carro e o índice da imagem
function HCimagCarro(iCarro, iImag) {
    return hCarros[iCarro].imags[iImag];
}

//Especificação do ano de lançamento de um carro, dado o seu indice
function HCespeciAnoCarro(iCarro) {
    return hCarros[iCarro].dados.ano;
}

//Especificação do peso em KG de lançamento de um carro, dado o seu indice
function HCespeciPesoCarro(iCarro) {
    return hCarros[iCarro].dados.peso;

}

//Especificação da cilindrada (cm3) de lançamento de um carro, dado o seu indice
function HCespeciCilCarro(iCarro) {
    return hCarros[iCarro].dados.cilindrada;
}

//Especificação do tipo de motor de lançamento de um carro, dado o seu indice
function HCespeciTipoMotorCarro(iCarro) {
    return hCarros[iCarro].dados.tipoMotor;
}

//Especificação da potencia maxima (cv) de um carro, dado o seu indice
function HCespeciPotMaxCarro(iCarro) {
    return hCarros[iCarro].dados.potencia;
}

//Especificação da rotação (rpm) da potencia maxima (cv) de um carro, dado o seu indice
function HCespeciRotPotCarro(iCarro) {
    return hCarros[iCarro].dados.rotPotencia;
}

//Especificação do binario maximo (kg, m) de um carro, dado o seu indice
function HCespeciBinCarro(iCarro) {
    return hCarros[iCarro].dados.binario;
}

//Especificação da rotação (rpm) do binario maximo de um carro, dado o seu indice
function HCespeciRotBinCarro(iCarro) {
    return hCarros[iCarro].dados.rotBinario;
}

//Especificação da velocidade maximo (km/h) de um carro, dado o seu indice
function HCespeciVelCarro(iCarro) {
    return hCarros[iCarro].dados.velMaxima;
}

//Especificação da aceleração maxima (duração em segundos do arranque 0-100 km/h) de um carro, dado o seu indice
function HCespeciAcelCarro(iCarro) {
    return hCarros[iCarro].dados.acel_0_100;
}

//Especificação da Comprimento (mm) de um carro, dado o seu indice
function HCespeciCompCarro(iCarro) {
    return hCarros[iCarro].dados.comp;
}

//Especificação da Largura (mm) de um carro, dado o seu indice
function HCespeciLargCarro(iCarro) {
    return hCarros[iCarro].dados.larg;
}

//Especificação da Largura (mm) de um carro, dado o seu indice
function HCespeciLargCarro(iCarro) {
    return hCarros[iCarro].dados.larg;
}

//Especificação da altura de um carro, dado o seu indice
function HCespeciAltCarro(iCarro) {
    return hCarros[iCarro].dados.alt;
}

// ********** testes de parsing************

//Texto de apresentação com indicação do número de hipercarros; testa as funções "HCnumCarros", "HCtituloApres",
// "HCnumParagsIntrod" e "HCparagIntrod" (tudo realtivo ao 1º objeto do array "hCarros").
function HCapresTeste() {
    var i, np = HCnumParagsIntrod();
    document.write('<h2>' + HCtituloApres() + '</h2>');
    document.write('<h3>(' + HCnumCarros() + ' hipercarros)</h3>');
    for (i = 0; i < np; i++) {
        document.write('<p>' + HCparagIntrod(i) + '</p>');
    }
    document.write('<hr />')
}

//Listagem geral dos hipercarros; testa as funções "MCmarcaModeloCarro", "HCversaoCarros", "HCparagCarro" e
// "HCimagCarro";
function HClistaGeralTeste() {
    var i, j, nc = HCnumCarros();
    for (i = 1; i <= nc; i++) {
        document.write(
            '<h3>' + HCmarcaModeloCarro(i) + ' (' + HCversaoCarros(i) + ') ' + '</h3>'
        );
        //Ilustrações
        for (j = 0; j < 4; j++) {
            document.write(
                '<img src="' + PATH + HCimagCarro(i, j) + '" width="200"/>'
            )
        }
        //Paragrafos e especificações
        document.write(
            '<p>' + HCparagCarro(i, 0) + '</p>' +
            '<p>' + HCparagCarro(i, 1) + '</p>' +
            HCcodTabEspecs(i)+
            '<hr />'
        )
    }
}

//gera o código de uma tabela de especificações técnicas e um carro, dado o seu indice
function HCcodTabEspecs(iCarro) {
    var especs=["Ano de Lançamento","Peso","Cilindrada","Tipo de Motor","Potencia Max","Rotação na Potencia Max","Binário MAx","Rotação no Binário Max","Velocidade Maxima","Aceleração Max","Comprimento","Largura","Altura"];
    var tabela = '<table border="1px">';
    //Ano
    tabela += '<tr><td>'+especs[0]+'</td><td>'+HCespeciAnoCarro(iCarro)+'</td></tr>';
    //Peso
    tabela += '<tr><td>'+especs[1]+'</td><td>'+HCespeciPesoCarro(iCarro)+'</td></tr>';
    //Cilindrada
    tabela += '<tr><td>'+especs[2]+'</td><td>'+HCespeciCilCarro(iCarro)+'</td></tr>';
    //Tipo de Motor
    tabela += '<tr><td>'+especs[3]+'</td><td>'+HCespeciTipoMotorCarro(iCarro)+'</td></tr>';
    //Potencia Max
    tabela += '<tr><td>'+especs[4]+'</td><td>'+HCespeciPotMaxCarro(iCarro)+'</td></tr>';
    //Rotação na Potencica Max
    tabela += '<tr><td>'+especs[5]+'</td><td>'+HCespeciRotPotCarro(iCarro)+'rpm</td></tr>';
    //Binario
    tabela += '<tr><td>'+especs[6]+'</td><td>'+HCespeciBinCarro(iCarro)+'</td></tr>';
    //Rotação do Binario
    tabela += '<tr><td>'+especs[7]+'</td><td>'+HCespeciRotBinCarro(iCarro)+'rpm</td></tr>';
    //Velociade Max
    tabela += '<tr><td>'+especs[8]+'</td><td>'+HCespeciVelCarro(iCarro)+'km/h</td></tr>';
    //Aceleração
    tabela += '<tr><td>'+especs[9]+'</td><td>'+HCespeciAcelCarro(iCarro)+'segs 0-100km/h</td></tr>';
    //Comprinento
    tabela += '<tr><td>'+especs[10]+'</td><td>'+HCespeciCompCarro(iCarro)+'mm</td></tr>';
    //Largura
    tabela += '<tr><td>'+especs[11]+'</td><td>'+HCespeciLargCarro(iCarro)+'mm</td></tr>';
    //Altura
    tabela += '<tr><td>'+especs[12]+'</td><td>'+HCespeciAltCarro(iCarro)+'mm</td></tr>';
    tabela += '</table>';
    return tabela;
}

