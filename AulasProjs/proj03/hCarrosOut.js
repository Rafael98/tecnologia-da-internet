//Proj03 : HIPERCARROS #3

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// ********** variáveis públicas **********
var interf = {};
var nCarro = 1; // numero do carro em apresentaçao
var numCarros = HCnumCarros(); //número de carros da lista

// *********** função principal ***********
function inic() {
    adqInterf();
    introd();
}

// ************** processos ***************
function adqInterf() {
    interf.apres = document.getElementById("apres");
    interf.titulo = document.getElementById("titulo");
    interf.txt1 = document.getElementById("txt1");
    interf.txt2 = document.getElementById("txt2");
    interf.carros = document.getElementById("carros");
    interf.refCarro = document.getElementById("refCarro");
    interf.txt2 = document.getElementById("txt2");
    interf.txt2 = document.getElementById("txt2");
    interf.txt3 = document.getElementById("txt3");
    interf.txt4 = document.getElementById("txt4");
    interf.figs = document.getElementById("figs");
    interf.especs = document.getElementById("especs");
    interf.figAmpl = document.getElementById("figAmpl");

}

//seleciona um novo carro (anterior ou seguinte)
function moveCarro(passo) {
    nCarro += passo;
    if (nCarro < 1) nCarro = numCarros;
    if (nCarro > numCarros) nCarro = 1;
    mostraCarros();
}

//escolhe um novo carro da lista de seleção da "listaSel"
function escolheCarro() {
    nCarro = interf.listaSel.selectedIndex +1;
    mostraCarros();
}

//gera o código HTML de uma lista de seleção dos carros
function codListaSelect() {
    var i, listSel = '<select id="listaSel" onchange="escolheCarro()">';
    for (i = 1; i <= numCarros; i++) {
        listSel += "<option>";
        listSel += i + '. ' + HCmarcaModeloCarro(i) + ' - ' + HCversaoCarro(i);
        listSel += "</option>";
    }
    listSel += '</select>';
    return listSel;
}

// ************* input/output *************
//apresenta a introduçao
function introd() {
    interf.titulo.innerHTML = HCtituloApres();
    interf.txt1.innerHTML =
        '<p>' + HCparagIntrod(0) + ' </p>' +
        '<p>' + HCparagIntrod(1) + ' </p>' +
        '<p>' + HCparagIntrod(2) + ' </p>';
    interf.txt2.innerHTML =
        '<p>' + HCparagIntrod(3) + ' </p>' +
        '<p>' + HCparagIntrod(4) + ' </p>' +
        codListaSelect() +
        '<p><button onclick="mostraCarros()">ver carro atual</button></p>';
    interf.listaSel = document.getElementById("listaSel");
}

//apresenta o atual mcarro
function mostraCarros() {
    var i, cod4Imgs = '';//codigo HTML para 4 Imagens
    for (i = 0; i < 4; i++) {
        cod4Imgs += '<img src="' + HCrefImagCarro(nCarro, i) + '"width="221" onclick="amplia(this)" />';
    }
    interf.carros.style.visibility = "visible";
    interf.figAmpl.style.visibility = "hidden";
    interf.apres.style.visibility = "hidden";
    interf.refCarro.innerHTML = nCarro + '. ' + HCmarcaModeloCarro(nCarro) + ' - ' + HCversaoCarro(nCarro);
    interf.txt3.innerHTML = HCparagCarro(nCarro, 0);
    interf.txt4.innerHTML = HCparagCarro(nCarro, 1);
    interf.figs.innerHTML = cod4Imgs;
    interf.especs.innerHTML = HCcodTabEspecs(nCarro);
}

//mostra a apresentaçao da introduçao
function mostraApres() {
    interf.carros.style.visibility = "hidden";
    interf.figAmpl.style.visibility = "hidden";
    interf.apres.style.visibility = "visible";
}

//mostra (no DIV "figAmpl") uma ampliação da imagem indicada
function amplia(figura) {
    interf.figAmpl.style.visibility = "visible";
    interf.figAmpl.innerHTML = '<img src="' + figura.src + '" /><br />' +
        '<b>' + nCarro + '. ' + HCmarcaModeloCarro(nCarro) + ' - ' + HCversaoCarro(nCarro) + '</b> (clique para fechar)';
}
