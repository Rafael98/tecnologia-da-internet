//Proj03 : HIPERCARROS #3

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

//Norma: segundo norma indicada pelo editor da base de dados, cada carro tem dois paragrafos descritivos e quatro imagens ilustrativas

// ********** variáveis públicas **********
    var PATH = "../../Recursos/hCarros/";
// *********** função principal ***********

// ************** processos de parsing ***************
//numero de super carros
function HCnumCarros(){
    return hCarros.length-1;
}
//titulo da aparesentaçao
function HCtituloApres(){
    return hCarros[0].titulo;
}
//numero de paragrafos da introduçao
function HCnumParagsIntrod(){
    return hCarros[0].apres.length;
}
//Texto de um paragrafo da introduçao dado o seu indice
function HCparagIntrod(iParag){
    return hCarros[0].apres[iParag];
}
//Marca/Modelo de um carro, dado o seu indice
function HCmarcaModeloCarro(iCarro){
    return hCarros[iCarro].marcaModelo;
}
// versao de um carro, dado o seu indice
function HCversaoCarro(iCarro){
    return hCarros[iCarro].versao;

}
//texto de um dos dois paragrafod decritivos de um carro dado o indice do carro e o indice do paragrafo
function HCparagCarro(iCarro,iParag){
    return hCarros[iCarro].texto[iParag];
}
//referencia completa a uma das quatro imagens ilustrativas de um carro, dado indice do carro e o indice da imagem
function HCrefImagCarro(iCarro,iImag){
    return PATH + hCarros[iCarro].imags[iImag];
}
//Especificaçoes do ano de lançamento de um carro, dado o seu indice
function HCespecAnoCarro(iCarro){
    return hCarros[iCarro].dados.ano;
}
//Especificaçoes do peso do carro, dado o seu indice
function HCespecPesoCarro(iCarro){
    return hCarros[iCarro].dados.peso;
}
//Especificaçoes da cilindrada de um carro, dado o seu indice
function HCespecCilCarro(iCarro){
    return hCarros[iCarro].dados.cilindrada;
}
//Especificaçoes do ano de lançamento de um carro, dado o seu indice
function HCespecTipoMotorCarro(iCarro){
    return hCarros[iCarro].dados.tipoMotor;
}
//Especificaçoes da potencia maxima (cv) de um carro, dado o seu indice
function HCespecPotCarro(iCarro){
    return hCarros[iCarro].dados.potencia;
}
//Especificaçoes da rotaçao (rpm) da potencia maxima de um carro, dado o seu indice
function HCespecRotPotCarro(iCarro){
    return hCarros[iCarro].dados.rotPotencia;
}
//Especificaçoes do binario Maximo (kg.m) de um carro, dado o seu indice
function HCespecBinCarro(iCarro){
    return hCarros[iCarro].dados.binario;
}
//Especificaçoes do rotaçao do binario maximo de um carro, dado o seu indice
function HCespecRotBinCarro(iCarro){
    return hCarros[iCarro].dados.rotBinario;
}
//Especificaçoes da velocidade maxima (km/h) de um carro, dado o seu indice
function HCespecVelCarro(iCarro){
    return hCarros[iCarro].dados.velMaxima;
}
//Especificaçoes da aceleraçao maxima (duraçao em segundos do arranque 0-100 km/h) de um carro, dado o seu indice
function HCespecAcelCarro(iCarro){
    return hCarros[iCarro].dados.acel_0_100;
}
//Especificaçoes do comprimento (mm) de um carro, dado o seu indice
function HCespecCompCarro(iCarro){
    return hCarros[iCarro].dados.comp;
}
//Especificaçoes da largura (mm) de um carro, dado o seu indice
function HCespecLargCarro(iCarro){
    return hCarros[iCarro].dados.larg;
}
//Especificaçoes da altura (mm) de um carro, dado o seu indice
function HCespecAltCarro(iCarro){
    return hCarros[iCarro].dados.alt;
}

// gera o codigo de uma tabela de especificaçoes tecnicas de um carro dado o seu indice
function HCcodTabEspecs(iCarro){
    var especs= ["Ano de Lançamento","Peso","Cilindrada","Tipo de Motor","Potencia Maxima","Rotacçao na Potencia Maxima","Binario Maximo","Rotaçao de Binario Maximo","Velocidade Maxima","Aceleraçao Maxima 0-100 Km/h","Comprimento","Largura","Altura"];
    var tabela= " <table>";
    //Ano
    tabela+='<tr><td class="etiq1">'+especs[0]+'</td><td class="valor">'+HCespecAnoCarro(iCarro)+'</td></tr>';

    //Peso
    tabela+='<tr><td class="etiq1">'+especs[1]+'</td><td class="valor">'+HCespecPesoCarro(iCarro)+' Kg</td></tr>';

    //Cilindrada
    tabela+='<tr><td class="etiq1">'+especs[2]+'</td><td class="valor">'+HCespecCilCarro(iCarro)+' cm3</td></tr>';

    //Tipo de Motor
    tabela+='<tr><td class="etiq1">'+especs[3]+'</td><td class="valor">'+HCespecTipoMotorCarro(iCarro)+'</td></tr>';

    //Potencia Maxima
    tabela+='<tr><td class="etiq1">'+especs[4]+'</td><td class="valor">'+HCespecPotCarro(iCarro)+' cv</td></tr>';

    //Rotaçao da Potentcia Maxima
    tabela+='<tr><td class="etiq1">'+especs[5]+'</td><td class="valor">'+HCespecRotPotCarro(iCarro)+' rpm</td></tr>';

    //Binario Maximo
    tabela+='<tr><td class="etiq1">'+especs[6]+'</td><td class="valor">'+HCespecBinCarro(iCarro)+' kg.m</td></tr>';

    //Rotaçao de Binario Maximo
    tabela+='<tr><td class="etiq1">'+especs[7]+'</td><td class="valor">'+HCespecRotBinCarro(iCarro)+' rpm</td></tr>';

    //Velocidade Maxima dos 0-100 Km/h
    tabela+='<tr><td class="etiq1">'+especs[8]+'</td><td class="valor">'+HCespecVelCarro(iCarro)+' km/h</td></tr>';

    // Aceleraçao Maxima
    tabela+='<tr><td class="etiq1">'+especs[9]+'</td><td class="valor">'+HCespecAcelCarro(iCarro)+' seg</td></tr>';

    //Comprimento
    tabela+='<tr><td class="etiq1">'+especs[10]+'</td><td class="valor">'+HCespecCompCarro(iCarro)+' mm</td></tr>';

    //Largura
    tabela+='<tr><td class="etiq1">'+especs[11]+'</td><td class="valor">'+HCespecLargCarro(iCarro)+' mm</td></tr>';

    //Altura
    tabela+='<tr><td class="etiq1">'+especs[12]+'</td><td class="valor">'+HCespecAltCarro(iCarro)+' mm</td></tr>';
    tabela +=" </table>";
    return tabela;
}

// *************** testes de parsing ***************
//Texto de apresentaçao com indicaçao do numero de hipercarros;testa as funcçoes "HCnumCarros"," HCtituloApres","HCnumParagsIntrod" e "HCparagIntrod" (tudo relativo ao primeiro objeto do array "hCarros")
function HCapresTeste(){
    var i,np= HCnumParagsIntrod();
    document.write("<h2>"+ HCtituloApres() +"</h2>");
    document.write("<h3>("+ HCnumCarros()  +" HiperCarros) </h3>");
    for(i=0;i<np;i++){
        document.write("<p>"+ HCparagIntrod(i) +"</p>");
    }
    document.write("<hr />");
}
//listagem geral dos hipercarros; testa as funçoes as anteriores e  "HCmarcaModeloCarro"," HCversaoCarro","HCparagCarro" e "HCimagCarro", teste de todas as specs

function HClistaGeralTeste(){
    var i, nc=HCnumCarros();
    for(i=1;i<=nc;i++){
        //cabeçalhos
        document.write(
            "<h3>" +
           HCmarcaModeloCarro(i) +
           " ( " + HCversaoCarro(i) + " )" +
            "</h3>"
        );
        //ilustraçoes
        for(j=0;j<4;j++){
            document.write(
                '<img src="'+PATH+HCrefImagCarro(i,j)+'" width="220" /> '
            );
        }
        //paragrafos,separador e especificaçoes
        document.write(
            "<p>" + HCparagCarro(i,0) + "</p>"+
            "<p>" + HCparagCarro(i,1) + "</p>"+
            HCcodTabEspecs(i)+
            "<hr />"
        );
    }

}
