//PRJ04: EXPRESSIONISMO ABSTRATO

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// ********** variáveis públicas **********
var PATH = "../../Recursos/exprAbstr/";

// *********** função principal ***********
function inic(){
	var i, np = numPintores();
	var j, no;
	for(i=0; i<np; i++){
		document.write(codNotaBioPintorTeste(i));
	}
	document.write(codApresIntrodTeste());
	for(i=0; i<np; i++){
		document.write(codBioPintorTeste(i));
		no = numObrasPintor(i);
		for(j=0; j<no; j++){
			document.write(fichaObraPintorTeste(i, j));
		}
	}
}

// ************** processos de parsing (parsing quer dizer intrepretar/traduzir)***************

//titulo da introdução
function tituloIntrod(){
	return exprAbstr.introd.titulo;
}

//subtítulo da introdução
function subtituloIntrod(){
	return exprAbstr.introd.subtitulo;
}

//Número de paragrafos da introdução
function numParagsIntrod(){
	return exprAbstr.introd.texto.length;
}

//Parágrafo da Introdução dado o seu indice
function ParagsIntrod(iParag){
	return exprAbstr.introd.texto[iParag];
}

//número de pintores representados
function numPintores(){
	return exprAbstr.pintores.length;
}

//nome de um pintor dado o seu indice
function nomePintor(iPintor){
	return exprAbstr.pintores[iPintor].nome;
}

//nacionalidade de um pintor dado o seu indice
function nacionPintor(iPintor){
	return exprAbstr.pintores[iPintor].nacion;
}

//data de nascimento (string "AAAAMMDD") de um pintor dado o seu indice
function dataNascPintor(iPintor){
	return exprAbstr.pintores[iPintor].dataNasc;
}

//local de nascimento de um pintor dado o seu indice
function localNascPintor(iPintor){
	return exprAbstr.pintores[iPintor].localNasc;
}

//data de morte (string "AAAAMMDD") de um pintor dado o seu indice
function dataMortePintor(iPintor){
	return exprAbstr.pintores[iPintor].dataMorte;
}

//local de morte de um pintor dado o seu indice
function localMortePintor(iPintor){
	return exprAbstr.pintores[iPintor].localMorte;
}

//referencia completa á foto do pintor dado o seu indice
function refFotoPintor(iPintor){
	return PATH + exprAbstr.pintores[iPintor].foto;
}

//numero de paragrafos do texto sobre o pintor dado o seu indice
function numParagsPintor(iPintor){
	return exprAbstr.pintores[iPintor].texto.length;
}

//paragrafo do texto sobre o pintor dado o seu indice do pintor e o indice do paragrafo
function paragPintor(iPintor,iParag){
return exprAbstr.pintores[iPintor].texto[iParag];
}

//numero de obras de um pintor dado o seu indice
function numObrasPintor(iPintor){
	return exprAbstr.pintores[iPintor].obras.length;
}

//titulo de uma obra de um pintor dado o indice do pintor e o indce da obra
function tituloObraPintor(iPintor,iObra){
	return exprAbstr.pintores[iPintor].obras[iObra].titl;
}

//ano de uma obra de um pintor dado o indice do pintor e o indce da obra
function anoObraPintor(iPintor,iObra){
	return exprAbstr.pintores[iPintor].obras[iObra].ano;
}

//referencia da imagem de uma obra de um pintor dado o indice do pintor e o indce da obra
function refImagObraPintor(iPintor,iObra){
	return PATH + exprAbstr.pintores[iPintor].obras[iObra].imag;
}

//texto de uma obra de um pintor dado o indice do pintor e o indce da obra
function textoObraPintor(iPintor,iObra){
	return exprAbstr.pintores[iPintor].obras[iObra].texto;
}

//data por extenso a partir do formato "AAAAMMDD"
function dataExt(str){
	var NMES = ["janeiro","fevereiro","março","abril","maio","junho","julho","agosto","setembro","outubro","novembro","dezembro"];
	var ano = str.substring(0, 4);
	var mes = parseInt(str.substring(4, 6), 10);
	var dia = parseInt(str.substring(6, 8), 10);
	return dia + " de " + NMES[mes-1] + " de " + ano;
}

// ************* testes de parsing *************

//código da nota biográfca com foto á esquerda de um pintor, dado o seu indice
function codNotaBioPintorTeste(iPintor) {
    var cod = "<table><tr>";
    cod +=
        '<td style="padding: 3px;">' +
			'<img src="' + refFotoPintor(iPintor) +
			'" width="170"/>' +
        '</td>'+
        '<td style="vertical-align: bottom; padding: 3px;font:15px/1.3 arial; width:430px;">' +
			'<b>' +nomePintor(iPintor)+ '</b>' +
			', pintor de nacionalidade ' + nacionPintor(iPintor) +
			', nascido a ' + dataExt(dataNascPintor(iPintor)) +
			', em ' + localNascPintor(iPintor) +
			', e falecido a ' + dataExt(dataMortePintor(iPintor)) +
			', em ' + localMortePintor(iPintor) +
        '</td>';
    cod += '</tr></table>';
    return cod;
}

//código da apresentação introdutória
function codApresIntrodTeste(){
	var i, np = numParagsIntrod();
	var cod = '<div style="width:550px padding:6px">';
	cod+= '<h1 style="font:bold 28px arial;">'+ tituloIntrod() + '</h1>';
	cod+= '<h2 style="font:bold 24px arial;">'+ subtituloIntrod() + '</h2>';
	for(i=0; i<np; i++){
		cod += '<p style="font:13px/1.4 verdana text-align:justify;"' + ParagsIntrod(i) + '</p>';
	}
	cod+= '</div>';
	return cod;
}

//código da biografia de um pintor dado o seu indice
function codBioPintorTeste(iPintor){
	var i, np = numParagsPintor(iPintor);
	var cod = codNotaBioPintorTeste(iPintor);
	cod+= '<div style="width:600px; padding:6px;">';
	cod += '<h3 style="font:bold 20px arial;">' + nomePintor(iPintor) +
	'</h3>';
	
	for(i=0; i<np; i++){
		cod += '<p style="font:14px/1.4 verdana; text-align:justify; ">' + paragPintor(iPintor, i) + '</p>';
	}
	cod+= '</div>';
	return cod;
}

//código da ficha d uma obra de um pintor dado o indice o pintor e o indice da obra dado o indice do pintor
function fichaObraPintorTeste(iPintor, iObra){
	var cod = '<table>';
	cod += '<tr>';
	cod += '<td width="400">';
	cod +=	'<img src="'+ refImagObraPintor(iPintor, iObra) +'" width="400" />';
	cod += '</td>';
  cod += '<td width="200" style="padding:3px; font:bold 15px/1.3 arial; vertical-align:bottom;">' + tituloObraPintor(iPintor, iObra) +
    ', ' + anoObraPintor(iPintor, iObra) +  '</td>';
  cod += '</tr>';
  cod += '<tr>';
  cod += '<td colspan="2" width="600" style="padding:3px; font:14px/1.4 verdana; text-align:justify;">' + textoObraPintor(iPintor, iObra) + '</td>';
  cod += '</tr>';
	cod += '</table>';
	return cod;
}