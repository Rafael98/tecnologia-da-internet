//PRJ06: SETE SERIES

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// ******** variáveis públicas ********
PATH = "../../Recursos/seteSeries/";

// ********* função principal *********
function xinic() {
    document.write(codTitlApres());
    document.write(codSeriesTeste());
}

// ************ processos *************
//titulo da apresentação
function tituloApres() {
    return seteSeriesTV.titulo;
}

//nº de series de TV na apresentação
function numSeries() {
    return seteSeriesTV.series.length;
}

//titulo de uma serie dado o seu indice
function tituloSerie(iSerie) {
    return seteSeriesTV.series[iSerie].titulo;
}

//referência de um banner de uma serie dado o seu indice
function refBannerSerie(iSerie) {
    return PATH + seteSeriesTV.series[iSerie].banner;
}

//nome do canal de uma serie dado o seu indice
function nomeCanalSerie(iSerie) {
    return seteSeriesTV.series[iSerie].canalTV.nome;
}

//url do canal de uma serie dado o seu indice
function urlCanalSerie(iSerie) {
    return seteSeriesTV.series[iSerie].canalTV.url;
}

//logo do canal de uma serie dado o seu indice
function refLogoCanalSerie(iSerie) {
    return PATH + seteSeriesTV.series[iSerie].canalTV.logo;
}

//dia da data de lançamento de uma serie dado o seu indice
function diaDataLancSerie(iSerie) {
    return parseInt(seteSeriesTV.series[iSerie].dataLanc.dia, 10);
}

//mes da data de lançamento de uma serie dado o seu indice
function mesDataLancSerie(iSerie) {
    return parseInt(seteSeriesTV.series[iSerie].dataLanc.mes, 10);
}

//ano da data de lançamento de uma serie dado o seu indice
function anoDataLancSerie(iSerie) {
    return parseInt(seteSeriesTV.series[iSerie].dataLanc.ano, 10);
}

//nº de criadores de uma serie dado o seu indice
function numCriadoresSerie(iSerie) {
    return seteSeriesTV.series[iSerie].criadores.length;
}

//nome de um criador de uma serie dado o seu indice da serie e do criador
function nomeCriadorSerie(iSerie, iCriad) {
    return seteSeriesTV.series[iSerie].criadores[iCriad];
}

//nº de generos de uma serie dado o seu indice
function numGenerosSerie(iSerie) {
    return seteSeriesTV.series[iSerie].generos.length;
}

//nome de um género de uma serie dado o seu indice da serie e do género
function nomeGenerosSerie(iSerie, iGenero) {
    return seteSeriesTV.series[iSerie].generos[iGenero];
}

//nº de conexões web de uma serie dado o seu indice
function numLinksSerie(iSerie) {
    return seteSeriesTV.series[iSerie].webLinks.length;
}

//nome de um site web de uma serie dado o seu indice da serie e do site
function nomeSiteWebSerie(iSerie, iSite) {
    return seteSeriesTV.series[iSerie].webLinks[iSite].site;
}

//url de um site web de uma serie dado o seu indice da serie e do site
function urlSiteWebSerie(iSerie, iSite) {
    return seteSeriesTV.series[iSerie].webLinks[iSite].url;
}

//referência completa da imagem do logotipo de um site web de uma serie dado o seu indice da serie e do site
function refLogoSiteWebSerie(iSerie, iSite) {
    return PATH + seteSeriesTV.series[iSerie].webLinks[iSite].logo;
}

//nº de atores de uma serie dado o seu indice
function numAtoresSerie(iSerie) {
    return seteSeriesTV.series[iSerie].elenco.length;
}

//nome do ator de uma serie dado o seu indice e do ator
function nomeAtoresSerie(iSerie, iAtor) {
    return seteSeriesTV.series[iSerie].elenco[iAtor];
}

//nº de paragrafos da sinopse de uma serie dado o seu indice
function numParagSinopseSerie(iSerie) {
    return seteSeriesTV.series[iSerie].sinopse.length;
}

//teor do paragrafo da sinopse de uma serie dado o seu indice e do paragrafo
function paragSinopseSerie(iSerie, iParag) {
    return seteSeriesTV.series[iSerie].sinopse[iParag];
}

//nº de temporadas de uma serie dado o seu indice
function numTemporadasSerie(iSerie) {
    return seteSeriesTV.series[iSerie].temporadas.length;
}

//nº de uma temporada de uma serie dado o seu indice e da temporada
function numTemporadaSerie(iSerie, iTempor) {
    return seteSeriesTV.series[iSerie].temporadas[iTempor].numero;
}

//nº de episodios de uma temporada de uma serie dado o seu indice e da temporada
function numEpisodiosTemporadaSerie(iSerie, iTempor) {
    return seteSeriesTV.series[iSerie].temporadas[iTempor].episodios;
}

//referência completa da capa de uma temporada de uma serie dado o seu indice e da temporada
function refCapaTemporadaSerie(iSerie, iTempor) {
    return PATH + seteSeriesTV.series[iSerie].temporadas[iTempor].capa;
}

//resumo de uma temporada de uma serie dado o seu indice e da temporada
function resumoTemporadaSerie(iSerie, iTempor) {
    return seteSeriesTV.series[iSerie].temporadas[iTempor].resumo;
}

// *********** input/output ***********

// *********** testes de parsing ***********
//codigo HTML do titulo da apresentação
function codTitlApres() {
    return '<h1>' + tituloApres() + '</h1>';
}

//codigo HTML de todas as series
function codSeriesTeste() {
    var i, ns = numSeries(), cod = '';
    for (i = 0; i < ns; i++) {
        cod += codBannerSerie(i);
        cod += codLinkCanalTV(i);
        cod += '<b>data de lançamento</b>:';
        cod += dataExtenso(
            diaDataLancSerie(i),
            mesDataLancSerie(i),
            anoDataLancSerie(i)
        ) + '<br />';
        cod += '<b>criadores</b>:' + codCriadoresSerie(i) + '<br />';
        cod += '<b>criadores</b>:' + codGenerosSerie(i) + '<br />';
        cod += codLinksSerie(i) + '<br />';
        cod += '<b>elenco</b>:' + codElencoSerie(i) + '<br />';
        cod += '<b>sinopse</b>:' + codSinopseSerie(i) + '<br />';
        cod += codTemporsSerie(i);
        cod += '<hr />';
    }
    return cod;
}

//codigo HTML da banner da serie dado o seu indice
function codBannerSerie(iSerie) {
    return '<img class="banner" src="' + refBannerSerie(iSerie) + '" />';
}

//codigo HTML da conexão para o canal de uma serie dado o seu indice
function codLinkCanalTV(iSerie) {
    var cod = '';
    cod += '<a class="linkCanal" href="' + urlCanalSerie(iSerie) + '" target="_blank">';
    cod += '<img class="logoCanal" src="' + refLogoCanalSerie(iSerie) + '" />';
    cod += '</a>';
    return cod;
}

//data por extenso dado o dia, o mes e o ano
function dataExtenso(dia, mes, ano) {
    var NM = ["janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"]
    return dia + ' de ' + NM[mes - 1] + ' de ' + ano;
}

//codigo HTML dos nomes dos criadores de uma serie dado o seu indice
function codCriadoresSerie(iSerie) {
    var i, nc = numCriadoresSerie(iSerie), cod = '';
    cod += '<p class="criads">';
    for (i = 0; i < nc; i++) {
        cod += nomeCriadorSerie(iSerie, i) + '; ';
    }
    cod += '</p>';
    return cod;
}

//codigo HTML dos generos dos criadores de uma serie dado o seu indice
function codGenerosSerie(iSerie) {
    var i, nc = numGenerosSerie(iSerie), cod = '';
    cod += '<p class="generos">';
    for (i = 0; i < nc; i++) {
        cod += nomeGenerosSerie(iSerie, i) + '; ';
    }
    cod += '</p>';
    return cod;
}

//codigo HTML das conexões de uma série dado o seu indice
function codLinksSerie(iSerie) {
    var i, nc = numLinksSerie(iSerie), cod = '';
    for (i = 0; i < nc; i++) {
        cod += '<a class="linkSerie" href="' + urlSiteWebSerie(iSerie, i) + '" target="_blank">';
        cod += '<img class="logoCanal" src="' + refLogoSiteWebSerie(iSerie, i) + '" />';
        cod += '</a>';
    }
    return cod;
}

//codigo HTML dos atores de uma serie dado o seu indice
function codElencoSerie(iSerie) {
    var i, na = numAtoresSerie(iSerie), cod = '';
    cod += '<p class="generos">';
    for (i = 0; i < na; i++) {
        cod += nomeAtoresSerie(iSerie, i) + '; ';
    }
    cod += '</p>';
    return cod;
}

//codigo HTML dos atores de uma serie dado o seu indice
function codSinopseSerie(iSerie) {
    var i, np = numParagSinopseSerie(iSerie), cod = '';
    cod += '<p class="generos">';
    for (i = 0; i < np; i++) {
        cod += paragSinopseSerie(iSerie, i) + '; ';
    }
    cod += '</p>';
    return cod;
}

function codTemporsSerie(iSerie) {
    var i, nt = numTemporadasSerie(iSerie), cod = '';
    for (i = 0; i < nt; i++) {
        cod += codTemporSerie(iSerie, i)
    }
    return cod;
}

//Código HTML de uma temporada de uma série, dado o indice da serie e o indice da temporada
function codTemporSerie(iSerie, iTempor) {
    var cod = '';
    cod += '<img class="capaTemp" src="' + refCapaTemporadaSerie(iSerie, iTempor) + '"/><br />';
    cod += '<b>Temporada nº</b>: ' + numTemporadaSerie(iSerie, iTempor) + '<br />';
    cod += '<b>nº de Episódios</b>: ' + numEpisodiosTemporadaSerie(iSerie, iTempor) + '<br />';
    cod += '<b>Resumo</b>: <p class="txtTemp">' + resumoTemporadaSerie(iSerie, iTempor) + '</p>';
    return cod;
}