//PRJ06: Sete Series

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// ********** variáveis públicas **********
var interf, numSerie;

// *********** função principal ***********
function inic() {
    numSerie = 2;
    interf = [];
    adqInterf();
    criaTitlLista();
    mostraSerie(numSerie - 1);
}

// ************** processos ***************
//adequire as referencias (encapsuladas como propriendades do objeto "interf") para a interface
function adqInterf() {
    interf.centro = document.getElementById("centro");
    interf.nomeSerie = document.getElementById("nomeSerie");
    interf.dataLanc = document.getElementById("dataLanc");
    interf.criadores = document.getElementById("criadores");
    interf.generos = document.getElementById("generos");
    interf.elenco = document.getElementById("elenco");
    interf.tituloApr = document.getElementById("tituloApr");
    interf.listaSeries = document.getElementById("listaSeries");
}

//movimenta a opção de avançar ou recuar na série atual
function move(p) {
    var i, ns = numSeries();
    numSerie += p;
    if (numSerie > ns) numSerie = 1;
    if (numSerie < 1) numSerie = ns;
    interf.listaSeries.selectedIndex = numSerie-1;
    mostraSerie(numSerie-1);
}

//procede à seleção
function escolhe() {
    numSerie = interf.listaSeries.selectedIndex;
    mostraSerie(numSerie);
}
// ************* input/output *************
//cria e mostra o titilo da apresentação e a letra de opções das séries
function criaTitlLista() {
    var i, ns = numSeries(), cod = '';
    interf.tituloApr.innerHTML = tituloApres();
    for (i = 0; i < ns; i++) {
        cod += '<option>' + tituloSerie(i) + '</option>';
    }
    interf.listaSeries.innerHTML = cod;
}

//mostra toda a informação de uma série, dado o indice da serie
function mostraSerie(iSerie) {
    mostraEsquerdo(iSerie);
    mostraCentro(iSerie);
}

//Apresenta as informações de uma séie, dado o indice da série
function mostraEsquerdo(iSerie) {
    var i, cod, nc = numCriadoresSerie(iSerie), ng = numGenerosSerie(iSerie), na = numAtoresSerie(iSerie);
    //nome da série
    interf.nomeSerie.innerHTML = tituloSerie(iSerie);
    interf.dataLanc.innerHTML = dataExtenso(diaDataLancSerie(iSerie), mesDataLancSerie(iSerie), anoDataLancSerie(iSerie));
    //criadores
    cod = '';
    for (i = 0; i < nc; i++) {
        cod += nomeCriadorSerie(iSerie, i) + '; ';
    }
    interf.criadores.innerHTML = cod;
    //generos
    cod = '';
    for (i = 0; i < ng; i++) {
        cod += nomeGenerosSerie(iSerie, i) + '; ';
    }
    interf.generos.innerHTML = cod;
    //elenco
    cod = '';
    for (i = 0; i < na; i++) {
        cod += nomeAtoresSerie(iSerie, i) + '; <br />';
    }
    interf.elenco.innerHTML = cod;
}

//Apresenta o logotipo e os links externos e a sinópse de uma séie, dado o indice da série
function mostraCentro(iSerie) {
    interf.centro.innerHTML = codCentro(iSerie);
}

//gera o código HTML para o logotipo e os links externos a sínopse de uma séie, dado o indice da série
function codCentro(iSerie) {
    var i, nc = numLinksSerie(iSerie), np = numParagSinopseSerie(iSerie), cod = '';
    //banner
    cod += '<img class="bannerSerie" src="' + refBannerSerie(iSerie) + '"/>';
    //link para o canal da TV
    cod += '<a class="linkSerie" href="' + urlCanalSerie(iSerie, i) + '" target="_blank">';
    cod += '<img class="imgLink" src="' + refLogoCanalSerie(iSerie, i) + '" alt="' + nomeCanalSerie(iSerie, i) + '"  />';
    cod += '</a>';
    //link da Série
    for (i = 0; i < nc; i++) {
        cod += '<a class="linkSerie" href="' + urlSiteWebSerie(iSerie, i) + '" target="_blank">';
        cod += '<img class="imgLink" src="' + refLogoSiteWebSerie(iSerie, i) + '" alt="' + nomeSiteWebSerie(iSerie, i) + '"  />';
        cod += '</a>';
    }
    for (i = 0; i < np; i++) {
        cod += '<p class="sinopse">' + paragSinopseSerie(iSerie, i) + '</p>'
    }
    return cod;
}