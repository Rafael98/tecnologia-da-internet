//PRJ05: EXPRESSIONISMO ABSTRATO

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// ********** variáveis públicas **********
var interf = {};
var numPintor = 0;

// *********** função principal ***********
function inic() {
    adqInterf();
    mostraIntrod();
    criaIntrod();
    //mostraPintor();
}

//adquire os elementos da apresentação no interface
function adqInterf() {
    interf.introd = document.getElementById("introd");
    interf.pintor = document.getElementById("pintor");
    interf.obra = document.getElementById("obra");
    interf.ctrl = document.getElementById("ctrl");
    interf.bio = document.getElementById("bio");
    interf.txtPintor = document.getElementById("txtPintor");
    interf.obras = document.getElementById("obras");
    interf.nPint = document.getElementById("nPint");
}

// ********* processos e controlo *********
//navega entre pintores
function move(p) {
    var np = numPintores();
    numPintor += p;
    if (numPintor >= np) numPintor = 0;
    if (numPintor < 0) numPintor = np - 1;
    mostraPintor();
}

// ************* input/output *************
//cria a introdução
function criaIntrod() {
    var i, cod = "", np = numParagsIntrod();
    cod += '<h1>' + tituloIntrod() + '</h1>';
    cod += '<h2>' + subtituloIntrod() + '</h2>';
    cod += '<p class="intro"><button onclick="mostraPintor()">ver pintores</button></p>';
    for (i = 0; i < np; i++) {
        cod += '<p class="intro">' + ParagsIntrod(i) + '</p>';
    }
    cod += '<p class="intro"><button onclick="mostraPintor()">ver pintores</button></p>';
    interf.introd.innerHTML = cod;
}

//mostra a introdução
function mostraIntrod() {
    interf.introd.style.display = "block";
    interf.pintor.style.display = "none";
    interf.obra.style.display = "none";
}

//apresenta o pintor atualmente escolhido
function mostraPintor() {
    interf.introd.style.display = "none";
    interf.pintor.style.display = "block";
    interf.obra.style.display = "none";
    interf.nPint.innerHTML = numPintor + 1;
    interf.bio.innerHTML = codNotaBioPintor();
    interf.txtPintor.innerHTML = codTxtPintor();
    interf.obras.innerHTML = codObras();
}

//gera o código para a nota biográfica do pintor atualmente escolhido
function codNotaBioPintor() {
    var cod;
    cod = '<img src="' + refFotoPintor(numPintor) + '" width="160"/>';
    cod += '<p class="bio">';
    cod += '<b>' + nomePintor(numPintor) + '</b>';
    cod += ', pintor de nacionalidade ' + nacionPintor(numPintor);
    cod += ', nascido a ' + dataExt(dataNascPintor(numPintor));
    cod += ', em ' + localNascPintor(numPintor);
    cod += ', e falecido a ' + dataExt(dataMortePintor(numPintor));
    cod += ', em ' + localMortePintor(numPintor);
    cod += '</p>';
    return cod;
}

//gera o código para os paragrafos do pintor atualmente escolhido
function codTxtPintor() {
    var i, cod = "", np = numParagsPintor(numPintor);
    cod += '<h3>' + nomePintor(numPintor) + '</h3>';
    for (i = 0; i < np; i++) {
        cod += '<p class="pintor">' + paragPintor(numPintor, i) + '</p>';
    }
    return cod;
}

//gera o código para as obras do pintor atualmente escolhido
function codObras() {
    var i, cod = "", no = numObrasPintor(numPintor);
    for (i = 0; i < no; i++) {
        cod += '<img src="' + refImagObraPintor(numPintor, i) + '" width="150" style="cursor: pointer"' +
            ' onclick="mostraObra(this)" \>';
        cod += '<div class="etiq3">' + tituloObraPintor(numPintor, i) + ', ' + anoObraPintor(numPintor, i) + '</div>';
    }
    return cod;
}

function mostraObra(imag) {
    var iObra, refImag = imag.src, tam = refImag.length;
    refImag = refImag.substr(tam - 9, tam);
    iObra = indObraPintor(numPintor, refImag);
    interf.obra.style.display = "block";
    interf.pintor.style.display = "none";
    interf.obra.innerHTML = codObraPintor(iObra);
    // alert(iObra);
}

function codObraPintor(iObra) {
    var cod = '';
    cod += '<img src="' + refImagObraPintor(numPintor, iObra) + '" width="700" style="cursor: pointer" onclick="mostraPintor()"/>';
    cod += '<h3>' + tituloObraPintor(numPintor, iObra) + ' (' + nomePintor(numPintor) + ', ' + anoObraPintor(numPintor, iObra) + ')</h3>';
    cod +='<p class="pintor">'+textoObraPintor(numPintor, iObra)+'</p>';
    return cod;
}

